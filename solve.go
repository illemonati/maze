package main

import (
	"log"
)

//Point : a point
type Point struct {
	x, y int
}

//StartSolve : start solve
func StartSolve(infile, outfile string, animated bool) chan [][]int {
	grid, err := Load(infile)
	if err != nil {
		log.Fatal(err)
	}
	c := make(chan [][]int)
	startPoint := findStartingPosition(grid)
	go solve(grid, c, startPoint, startPoint, false, false, animated)
	return c
}

func findStartingPosition(grid [][]int) Point {
	for y, row := range grid {
		for x, node := range row {
			if node == 2 {
				return Point{x, y}
			}
		}
	}
	return Point{0, 0}
}

func solve(grid [][]int, c chan [][]int, currentPoint, previousPoint Point, solved, dead, animated bool) ([][]int, bool, bool) {
	node := grid[currentPoint.y][currentPoint.x]
	var neighbors []Point
	if node != 3 {
		if currentPoint != previousPoint {
			grid[currentPoint.y][currentPoint.x] = 4
		}
		for neighbor := range findNeighbors(grid, currentPoint, previousPoint) {
			neighbors = append(neighbors, neighbor)
		}
	} else {
		grid[previousPoint.y][previousPoint.x] = 5
		solved = true
		log.Println("solved")
		Save(outfile, grid)
	}

	if animated {
		c <- grid
	} else {
		select {
		case c <- grid:
			//
		default:
			//
		}
	}

	if len(neighbors) == 0 && !solved {
		dead = true
	}
	allNeighborsDead := true
	for _, neighbor := range neighbors {
		grid, solved, dead = solve(grid, c, neighbor, currentPoint, false, dead, animated)
		if !dead || solved {
			allNeighborsDead = false
		}
		if solved {
			grid[previousPoint.y][previousPoint.x] = 5
			break
		}

	}
	if allNeighborsDead && !solved {
		grid[currentPoint.y][currentPoint.x] = 6
	}
	if currentPoint == previousPoint {
		c <- grid
		close(c)
		grid[currentPoint.y][currentPoint.x] = 2
	}
	return grid, solved, dead
}

func findNeighbors(grid [][]int, point, previousPoint Point) chan Point {
	c := make(chan Point)
	go func() {
		defer close(c)
		gridHeight := len(grid)
		gridWidth := len(grid[0])
		if point.x == 0 && point.y == 0 {
			checkNeighbors(c, grid, previousPoint, Point{0, 1}, Point{1, 0})
		} else if point.x == 0 {
			checkNeighbors(c, grid, previousPoint, Point{0, point.y - 1}, Point{1, point.y})
			if point.y < gridHeight-1 {
				checkNeighbors(c, grid, previousPoint, Point{0, point.y + 1})
			}
		} else if point.y == 0 {
			checkNeighbors(c, grid, previousPoint, Point{point.x, 1}, Point{point.x - 1, 0})
			if point.x < gridWidth-1 {
				checkNeighbors(c, grid, previousPoint, Point{point.x + 1, 0})
			}
		} else {
			checkNeighbors(c, grid, previousPoint, Point{point.x - 1, point.y}, Point{point.x, point.y - 1})
			if point.x < gridWidth-1 {
				checkNeighbors(c, grid, previousPoint, Point{point.x + 1, point.y})
			}
			if point.y < gridHeight-1 {
				checkNeighbors(c, grid, previousPoint, Point{point.x, point.y + 1})
			}
		}
	}()
	return c
}

func checkNeighbors(c chan Point, grid [][]int, previousPoint Point, points ...Point) {
	for _, point := range points {
		if grid[point.y][point.x] == 0 || grid[point.y][point.x] == 3 || grid[point.y][point.x] == 6 {
			if !((point.x == previousPoint.x) && (point.y == previousPoint.y)) {
				c <- point
			}
		}
	}
}
