package main

import (
	"bufio"
	"os"
	"strconv"
)

//Load : Load a maze
func Load(filepath string) ([][]int, error) {
	var grid [][]int

	file, err := os.Open(filepath)
	if err != nil {
		return grid, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		r := make([]int, len(line))
		for i, n := range line {
			number, err := strconv.Atoi(string(n))
			if err != nil {
				return grid, err
			}
			r[i] = number
		}
		grid = append(grid, r)
	}
	return grid, nil
}

//Save : Save a maze
func Save(filepath string, grid [][]int) {
	if len(filepath) == 0 {
		return
	}
	file, _ := os.Create(filepath)
	defer file.Close()
	for _, row := range grid {
		for _, node := range row {
			file.WriteString(strconv.Itoa(node))
		}
		file.WriteString("\n")
	}
}
