# solve maze

Can switch between instant and animated mode

Can disable GUI

Red means no path

animated is choppy as gif but much smoother when running

![static](https://gitlab.com/illemonati/maze/raw/master/pictures/staticred.png)

![animated](https://gitlab.com/illemonati/maze/raw/master/pictures/animatedred.gif)
