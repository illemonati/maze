package main

import (
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	input   = kingpin.Arg("input", "file path for the input file").Required().String()
	animate = kingpin.Flag("animate", "animate the solving process").Short('a').Default("false").Bool()
	output  = kingpin.Arg("output", "file path for the output file").String()
	nogui   = kingpin.Flag("no-gui", "disable gui").Short('x').Default("false").Bool()
)

func main() {
	kingpin.Version("0.01")
	kingpin.Parse()
	if !*nogui {
		startGui(*input, *output, *animate)
	} else {
		c := StartSolve(*input, *output, false)
		var grid [][]int
		for range c {
			grid = <-c
		}
		Save(*output, grid)
	}
}
