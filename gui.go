package main

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

const (
	//WIDTH : width of window
	WIDTH int = 1024
	//HEIGHT : height of window
	HEIGHT int = 800
)

var (
	infile   string
	outfile  string
	animated bool
)

func startGui(in, out string, a bool) {
	infile = in
	outfile = out
	animated = a
	pixelgl.Run(run)
}

func run() {

	cfg := pixelgl.WindowConfig{
		Title:  "Solve Maze",
		Bounds: pixel.R(0, 0, float64(WIDTH), float64(HEIGHT)),
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}
	for grid := range StartSolve(infile, outfile, animated) {
		drawGrid(grid, win)
		win.Update()
	}
	for !win.Closed() {
		win.Update()
	}
}

func drawGrid(grid [][]int, win *pixelgl.Window) {
	win.Clear(colornames.Black)
	blockHeight := HEIGHT / len(grid)
	blockWidth := WIDTH / len(grid[0])
	for y, row := range grid {
		for x, block := range row {
			imd := drawSingle(x*blockWidth, HEIGHT-y*blockHeight, blockWidth, -blockHeight, block)
			imd.Draw(win)
		}
	}
}

func drawSingle(startX, startY, blockWidth, blockHeight, blockType int) *imdraw.IMDraw {
	imd := imdraw.New(nil)
	switch blockType {
	case 0:
		imd.Color = colornames.White
	case 1:
		imd.Color = colornames.Black
	case 2:
		imd.Color = colornames.Blueviolet
	case 3:
		imd.Color = colornames.Blue
	case 4:
		imd.Color = colornames.Orange
	case 5:
		imd.Color = colornames.Green
	case 6:
		imd.Color = colornames.Red
	}
	imd.Push(pixel.V(float64(startX), float64(startY)), pixel.V(float64(startX+blockWidth), float64(startY+blockHeight)))
	imd.Rectangle(0)
	return imd
}
